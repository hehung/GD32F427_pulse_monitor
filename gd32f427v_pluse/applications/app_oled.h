/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-12-03     hehung       the first version
 */

#ifndef APP_OLED_H_
#define APP_OLED_H_

#include "stdint.h"


#define I2C_SLAVE_OLED_ADDRESS7   0x78

extern void OLED_Init(void);

extern void OLED_Fill(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t y2,uint8_t dot);
extern void OLED_Clear(void);

extern void OLED_Refresh_Gram(void);
extern void OLED_DrawPoint(uint8_t x,uint8_t y,uint8_t t);
extern void OLED_ShowHz(uint8_t x, uint8_t y, const char hz_c[], uint8_t mode);
extern void OLED_ShowChar(uint8_t x,uint8_t y,uint8_t chr,uint8_t size,uint8_t mode);
extern void OLED_ShowNum(uint8_t x, uint8_t y, uint32_t num, uint8_t len, uint8_t size, uint8_t mode);
extern void OLED_ShowString(uint8_t x,uint8_t y,const uint8_t *p,uint8_t size,uint8_t mode);
extern void OLED_ShowBMP(uint8_t x,uint8_t y,uint8_t mode,uint8_t bmp[][16],uint32_t len);
extern void OLED_ShowHz(uint8_t x, uint8_t y, const char hz_c[], uint8_t mode);
extern void OLED_ShowHzStringRow(uint8_t x, uint8_t y, const char *hz_s, uint8_t mode);
extern void OLED_ShowHzStringColumn(uint8_t x, uint8_t y, const char *hz_s, uint8_t mode);
extern void OLED_ShowHzFromIndex(uint8_t x, uint8_t y, uint8_t chr, uint8_t mode);

#endif /* APP_OLED_H_ */
