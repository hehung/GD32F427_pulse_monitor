/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-12-07     hehung       the first version
 */
#ifndef APP_ADC_H_
#define APP_ADC_H_

extern void Adc_Init(void);
extern uint16_t Adc_ChSample(uint8_t channel);

#endif /* APPLICATIONS_APP_ADC_H_ */
