/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-12-11     hehung       the first version
 */
#ifndef APP_COMMON_H_
#define APP_COMMON_H_

#define COMM_OLED                    (0U)
#define COMM_TFTLCD                  (1U)

#define COMM_DISPLAY_MODE            (COMM_TFTLCD)

#define COMMON_ON                    (1U)
#define COMMON_OFF                   (0U)

/* LVGL line使用起来有点问题，所以切换了原始的方式，自己画点显示波形图 */
#define COMMON_USE_LVGL              (COMMON_OFF)

#endif /* APP_COMMON_H_ */
