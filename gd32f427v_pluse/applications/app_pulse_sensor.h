/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-012-14     hehung       the first version
 */
#ifndef APP_PLUSE_SENSOR_H_
#define APP_PLUSE_SENSOR_H_

#include <stdint.h>
#include <stdbool.h>
#include "lvgl.h"

#define PS_WAVE_POINT_NUM                (320U)


extern void Ps_TaskCreate(void);
extern uint16_t Ps_GetRawAdcValue(void);
#if (COMMON_USE_LVGL == COMMON_OFF)
extern uint16_t* Ps_GetWaveformList(void);
#else
extern lv_point_t* Ps_GetWaveformList(void);
#endif
extern bool Ps_GetQsFlg(void);
extern void Ps_ClrQsFlg(void);
extern int Ps_GetBpm(void);
extern void Ps_TaskDelete(void);
extern void Timer_Init(void);


#endif /* SRC_APP_PLUSE_SENSOR_H_ */
