#include <app_pulse_sensor.h>
#include <app_tftlcd_drv.h>
/*
 * Copyright (c) 2006-2022, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-08-20     BruceOu      first implementation
 */

#include <stdio.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#include "app_hw_i2c.h"
#include "app_oled.h"
#include "app_adc.h"
#include <stdio.h>

#include "app_tftlcd_drv.h"
#include "gd32f4xx.h"
#include "lvgl.h"
#include "app_gui.h"


/* defined the LED2 pin: PC6 */
#define LED2_PIN GET_PIN(C, 6)

int main(void)
{
    /* Initialize the ADC */
    Adc_Init();
    /* Set interrupt priority group */
    nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
    Timer_Init();
#if (COMMON_USE_LVGL == COMMON_OFF)
    /* Initialize the GUI */
    Gui_Init();
#endif
    Ps_TaskCreate();

    /* set LED2 pin mode to output */
//    rt_pin_mode(LED2_PIN, PIN_MODE_OUTPUT);

    while (1)
    {
        Gui_PulseDispRefresh();
        rt_thread_mdelay(30);
    }

    return RT_EOK;
}
