#include <app_pulse_sensor.h>
/*
 * @hehung
 * 2022-12-13
 *  转载请注明出处
 * */
#if (COMMON_USE_LVGL == COMMON_ON)
#include "lvgl.h"
#endif
#include "app_gui.h"
#include "app_tftlcd_drv.h"
#include "app_common.h"
#include <stdio.h>

#if (COMMON_USE_LVGL == COMMON_ON)
static lv_obj_t *info_label;
static lv_obj_t *pulse_label;
static lv_obj_t *pulse_line;
static lv_point_t *pulse_data_point;
static lv_style_t pulse_line_style;
#else
static uint16_t *pulse_data_point;
#endif

#if (COMMON_USE_LVGL == COMMON_ON)
static void Gui_InfoLabel(void);
static void Gui_PulseDispInit(void);
#else
static void Gui_MainFunction(void);
static uint16_t Gui_GetNumMax(uint16_t data1, uint16_t data2);
static uint16_t Gui_GetNumMin(uint16_t data1, uint16_t data2);
#endif

#if (COMMON_USE_LVGL == COMMON_ON)
/* My GUI entry */
void Gui_LvglEntry(void)
{
    Gui_InfoLabel();
    Gui_PulseDispInit();
}

/* Lvgl test for porting to GD32F427V  */
static void Gui_InfoLabel(void)
{
    info_label = lv_label_create(lv_scr_act());
    lv_label_set_long_mode(info_label, LV_LABEL_LONG_WRAP);     /*Break the long lines*/
    lv_label_set_recolor(info_label, true);                      /*Enable re-coloring by commands in the text*/
    lv_label_set_text(info_label, "#0000ff GD32F427V-START | aijishu.com | hehung#");
    lv_obj_set_width(info_label, 320);  /*Set smaller width to make the lines wrap*/
    lv_obj_set_style_text_align(info_label, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_align(info_label, LV_ALIGN_TOP_MID, 0, 0);
}

/* Display for pulse sensor */
static void Gui_PulseDispInit(void)
{
    pulse_label = lv_label_create(lv_scr_act());
    lv_label_set_long_mode(pulse_label, LV_LABEL_LONG_WRAP);     /*Break the long lines*/
    lv_label_set_recolor(pulse_label, true);                      /*Enable re-coloring by commands in the text*/
    lv_label_set_text(pulse_label, "");
    lv_obj_set_width(pulse_label, 320);  /*Set smaller width to make the lines wrap*/
    lv_obj_set_style_text_align(pulse_label, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_align(pulse_label, LV_ALIGN_TOP_MID, 10, 16);

    lv_style_init(&pulse_line_style);
    lv_style_set_line_width(&pulse_line_style, 1);  // 设置线宽
    lv_style_set_line_color(&pulse_line_style, lv_palette_main(LV_PALETTE_BLUE));//设置线条颜色为蓝色
    lv_style_set_line_rounded(&pulse_line_style, true); // 使能线条圆角功能
    lv_style_set_width(&pulse_line_style, 320);
    lv_style_set_height(&pulse_line_style, 200);

    pulse_line = lv_line_create(lv_scr_act());  // 创建线条对象
    lv_line_set_points(pulse_line, pulse_data_point, PS_WAVE_POINT_NUM); // 设置线条数据点数
    lv_obj_add_style(pulse_line, &pulse_line_style, 0); // 线条添加style
    lv_obj_center(pulse_line);  // 居中显示
}
#else

void Gui_Init(void)
{
    /* Initialize lcd */
    lcd_init();
    lcd_clear(WHITE);

    lcd_draw_font_gbk16(5, 0, BLUE, WHITE, "GD32F427V-START | aijishu.com | hehung");
}

static uint16_t Gui_GetNumMax(uint16_t data1, uint16_t data2)
{
    return (data1 < data2) ? (data2) : (data1);
}

static uint16_t Gui_GetNumMin(uint16_t data1, uint16_t data2)
{
    return (data1 < data2) ? (data1) : (data2);
}

static void Gui_MainFunction(void)
{
    char bpm_str[20];
    uint16_t x;
    uint16_t y;

    sprintf(bpm_str, "BPM:%d     ", Ps_GetBpm());
    lcd_draw_font_gbk16(5, 16, RED, WHITE, bpm_str);

    pulse_data_point = Ps_GetWaveformList();
    /* 显示波形 */
    LCD_CS_CLR;
    for (x = 0; x < PS_WAVE_POINT_NUM; x++)
    {
        for (y = 0; y < 200; y++)
        {
            if (pulse_data_point[x] == y)
            {
                lcd_draw_point(x, y+40, BLUE);
            }
            else if ((x != (PS_WAVE_POINT_NUM-1)) && (y > Gui_GetNumMin(pulse_data_point[x], pulse_data_point[x+1])) &&
                    (y < Gui_GetNumMax(pulse_data_point[x], pulse_data_point[x+1])))
            {
//                rt_kprintf("test:%d\n",y);
                lcd_draw_point(x, y+40, BLUE);
            }
            else
            {
                lcd_draw_point(x, y+40, WHITE);
            }
        }
    }
    LCD_CS_SET;
}
#endif

/* Refresh display */
void Gui_PulseDispRefresh(void)
{
#if (COMMON_USE_LVGL == COMMON_ON)
    pulse_data_point = Ps_GetWaveformList();

    /* 这种方式显示出来有断层，暂时没找到原因 */
    lv_line_set_points(pulse_line, pulse_data_point, PS_WAVE_POINT_NUM); // 设置线条数据点数

    lv_label_set_text_fmt(pulse_label, "#ff0000 BMP:%d", Ps_GetBpm());
#else
    Gui_MainFunction();
#endif
}

