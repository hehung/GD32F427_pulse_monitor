/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-12-03     hehung       the first version
 */

#ifndef APP_HW_I2C_H_
#define APP_HW_I2C_H_

extern void i2c_gpio_config(void);
extern void i2c_config(void);

#endif /* APP_OLED_H_ */
