################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_adc.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_can.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_crc.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_ctc.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dac.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dbg.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dci.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dma.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_enet.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_exmc.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_exti.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_fmc.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_fwdgt.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_gpio.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_i2c.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_ipa.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_iref.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_misc.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_pmu.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_rcu.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_rtc.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_sdio.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_spi.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_syscfg.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_timer.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_tli.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_trng.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_usart.c \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_wwdgt.c 

O_SRCS += \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_adc.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_can.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_crc.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_ctc.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dac.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dbg.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dci.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dma.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_enet.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_exmc.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_exti.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_fmc.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_fwdgt.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_gpio.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_i2c.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_ipa.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_iref.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_misc.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_pmu.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_rcu.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_rtc.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_sdio.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_spi.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_syscfg.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_timer.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_tli.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_trng.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_usart.o \
../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_wwdgt.o 

OBJS += \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_adc.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_can.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_crc.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_ctc.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dac.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dbg.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dci.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dma.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_enet.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_exmc.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_exti.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_fmc.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_fwdgt.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_gpio.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_i2c.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_ipa.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_iref.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_misc.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_pmu.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_rcu.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_rtc.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_sdio.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_spi.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_syscfg.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_timer.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_tli.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_trng.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_usart.o \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_wwdgt.o 

C_DEPS += \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_adc.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_can.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_crc.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_ctc.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dac.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dbg.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dci.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_dma.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_enet.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_exmc.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_exti.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_fmc.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_fwdgt.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_gpio.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_i2c.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_ipa.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_iref.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_misc.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_pmu.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_rcu.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_rtc.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_sdio.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_spi.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_syscfg.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_timer.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_tli.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_trng.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_usart.d \
./libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/gd32f4xx_wwdgt.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/%.o: ../libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source/%.c
	arm-none-eabi-gcc -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\applications" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\board" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\libraries\GD32F4xx_Firmware_Library\CMSIS\GD\GD32F4xx\Include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\libraries\GD32F4xx_Firmware_Library\CMSIS" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\libraries\GD32F4xx_Firmware_Library\GD32F4xx_standard_peripheral\Include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\libraries\gd32_drivers" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\env_support\rt-thread" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\core" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\arm2d" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\gd32_ipa" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\nxp\pxp" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\nxp\vglite" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\nxp" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\sdl" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\stm32_dma2d" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\swm341_dma2d" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\sw" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\font" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\hal" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\layouts\flex" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\layouts\grid" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\barcode" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\bmp" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\ffmpeg" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\freetype" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\fsdrv" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\gif" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\png" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\qrcode" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\rlottie" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\sjpg" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\tiny_ttf" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\misc" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\file_explorer" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\fragment" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\gridnav" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\ime" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\imgfont" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\monkey" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\msg" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\snapshot" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\themes\basic" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\themes\default" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\themes\mono" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\themes" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\animimg" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\arc" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\bar" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\btnmatrix" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\btn" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\calendar" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\canvas" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\chart" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\checkbox" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\colorwheel" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\dropdown" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\imgbtn" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\img" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\keyboard" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\label" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\led" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\line" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\list" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\menu" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\meter" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\msgbox" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\objx_templ" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\roller" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\slider" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\span" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\spinbox" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\spinner" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\switch" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\table" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\tabview" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\textarea" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\tileview" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\win" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\drivers\include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\finsh" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\compilers\common\include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\compilers\newlib" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\posix\io\poll" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\posix\io\stdio" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\posix\ipc" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\libcpu\arm\common" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\libcpu\arm\cortex-m4" -include"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rtconfig_preinc.h" -std=gnu11 -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -Dgcc -O0 -gdwarf-2 -g -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

