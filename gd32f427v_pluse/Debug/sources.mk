################################################################################
# 自动生成的文件。不要编辑！
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
OBJS := 
SECONDARY_FLASH := 
SECONDARY_SIZE := 
ASM_DEPS := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
applications \
board \
libraries/GD32F4xx_Firmware_Library/CMSIS/GD/GD32F4xx/Source/GCC \
libraries/GD32F4xx_Firmware_Library/CMSIS/GD/GD32F4xx/Source \
libraries/GD32F4xx_Firmware_Library/GD32F4xx_standard_peripheral/Source \
libraries/gd32_drivers \
packages/LVGL-latest/env_support/rt-thread \
packages/LVGL-latest/src/core \
packages/LVGL-latest/src/draw/arm2d \
packages/LVGL-latest/src/draw/gd32_ipa \
packages/LVGL-latest/src/draw \
packages/LVGL-latest/src/draw/nxp \
packages/LVGL-latest/src/draw/nxp/pxp \
packages/LVGL-latest/src/draw/nxp/vglite \
packages/LVGL-latest/src/draw/sdl \
packages/LVGL-latest/src/draw/stm32_dma2d \
packages/LVGL-latest/src/draw/sw \
packages/LVGL-latest/src/draw/swm341_dma2d \
packages/LVGL-latest/src/font \
packages/LVGL-latest/src/hal \
packages/LVGL-latest/src/layouts/flex \
packages/LVGL-latest/src/layouts/grid \
packages/LVGL-latest/src/libs/barcode \
packages/LVGL-latest/src/libs/bmp \
packages/LVGL-latest/src/libs/ffmpeg \
packages/LVGL-latest/src/libs/freetype \
packages/LVGL-latest/src/libs/fsdrv \
packages/LVGL-latest/src/libs/gif \
packages/LVGL-latest/src/libs/png \
packages/LVGL-latest/src/libs/qrcode \
packages/LVGL-latest/src/libs/rlottie \
packages/LVGL-latest/src/libs/sjpg \
packages/LVGL-latest/src/libs/tiny_ttf \
packages/LVGL-latest/src/misc \
packages/LVGL-latest/src/others/file_explorer \
packages/LVGL-latest/src/others/fragment \
packages/LVGL-latest/src/others/gridnav \
packages/LVGL-latest/src/others/ime \
packages/LVGL-latest/src/others/imgfont \
packages/LVGL-latest/src/others/monkey \
packages/LVGL-latest/src/others/msg \
packages/LVGL-latest/src/others/snapshot \
packages/LVGL-latest/src/themes/basic \
packages/LVGL-latest/src/themes/default \
packages/LVGL-latest/src/themes/mono \
packages/LVGL-latest/src/widgets/animimg \
packages/LVGL-latest/src/widgets/arc \
packages/LVGL-latest/src/widgets/bar \
packages/LVGL-latest/src/widgets/btn \
packages/LVGL-latest/src/widgets/btnmatrix \
packages/LVGL-latest/src/widgets/calendar \
packages/LVGL-latest/src/widgets/canvas \
packages/LVGL-latest/src/widgets/chart \
packages/LVGL-latest/src/widgets/checkbox \
packages/LVGL-latest/src/widgets/colorwheel \
packages/LVGL-latest/src/widgets/dropdown \
packages/LVGL-latest/src/widgets/img \
packages/LVGL-latest/src/widgets/imgbtn \
packages/LVGL-latest/src/widgets/keyboard \
packages/LVGL-latest/src/widgets/label \
packages/LVGL-latest/src/widgets/led \
packages/LVGL-latest/src/widgets/line \
packages/LVGL-latest/src/widgets/list \
packages/LVGL-latest/src/widgets/menu \
packages/LVGL-latest/src/widgets/meter \
packages/LVGL-latest/src/widgets/msgbox \
packages/LVGL-latest/src/widgets/objx_templ \
packages/LVGL-latest/src/widgets/roller \
packages/LVGL-latest/src/widgets/slider \
packages/LVGL-latest/src/widgets/span \
packages/LVGL-latest/src/widgets/spinbox \
packages/LVGL-latest/src/widgets/spinner \
packages/LVGL-latest/src/widgets/switch \
packages/LVGL-latest/src/widgets/table \
packages/LVGL-latest/src/widgets/tabview \
packages/LVGL-latest/src/widgets/textarea \
packages/LVGL-latest/src/widgets/tileview \
packages/LVGL-latest/src/widgets/win \
rt-thread/components/drivers/i2c \
rt-thread/components/drivers/ipc \
rt-thread/components/drivers/misc \
rt-thread/components/drivers/serial \
rt-thread/components/finsh \
rt-thread/components/libc/compilers/common \
rt-thread/components/libc/compilers/newlib \
rt-thread/libcpu/arm/common \
rt-thread/libcpu/arm/cortex-m4 \
rt-thread/src \

