################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../packages/LVGL-latest/src/font/lv_font.c \
../packages/LVGL-latest/src/font/lv_font_dejavu_16_persian_hebrew.c \
../packages/LVGL-latest/src/font/lv_font_fmt_txt.c \
../packages/LVGL-latest/src/font/lv_font_loader.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_10.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_12.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_12_subpx.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_14.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_16.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_18.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_20.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_22.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_24.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_26.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_28.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_28_compressed.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_30.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_32.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_34.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_36.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_38.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_40.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_42.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_44.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_46.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_48.c \
../packages/LVGL-latest/src/font/lv_font_montserrat_8.c \
../packages/LVGL-latest/src/font/lv_font_simsun_16_cjk.c \
../packages/LVGL-latest/src/font/lv_font_unscii_16.c \
../packages/LVGL-latest/src/font/lv_font_unscii_8.c 

OBJS += \
./packages/LVGL-latest/src/font/lv_font.o \
./packages/LVGL-latest/src/font/lv_font_dejavu_16_persian_hebrew.o \
./packages/LVGL-latest/src/font/lv_font_fmt_txt.o \
./packages/LVGL-latest/src/font/lv_font_loader.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_10.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_12.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_12_subpx.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_14.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_16.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_18.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_20.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_22.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_24.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_26.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_28.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_28_compressed.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_30.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_32.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_34.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_36.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_38.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_40.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_42.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_44.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_46.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_48.o \
./packages/LVGL-latest/src/font/lv_font_montserrat_8.o \
./packages/LVGL-latest/src/font/lv_font_simsun_16_cjk.o \
./packages/LVGL-latest/src/font/lv_font_unscii_16.o \
./packages/LVGL-latest/src/font/lv_font_unscii_8.o 

C_DEPS += \
./packages/LVGL-latest/src/font/lv_font.d \
./packages/LVGL-latest/src/font/lv_font_dejavu_16_persian_hebrew.d \
./packages/LVGL-latest/src/font/lv_font_fmt_txt.d \
./packages/LVGL-latest/src/font/lv_font_loader.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_10.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_12.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_12_subpx.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_14.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_16.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_18.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_20.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_22.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_24.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_26.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_28.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_28_compressed.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_30.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_32.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_34.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_36.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_38.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_40.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_42.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_44.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_46.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_48.d \
./packages/LVGL-latest/src/font/lv_font_montserrat_8.d \
./packages/LVGL-latest/src/font/lv_font_simsun_16_cjk.d \
./packages/LVGL-latest/src/font/lv_font_unscii_16.d \
./packages/LVGL-latest/src/font/lv_font_unscii_8.d 


# Each subdirectory must supply rules for building sources it contributes
packages/LVGL-latest/src/font/%.o: ../packages/LVGL-latest/src/font/%.c
	arm-none-eabi-gcc -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\applications" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\board" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\libraries\GD32F4xx_Firmware_Library\CMSIS\GD\GD32F4xx\Include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\libraries\GD32F4xx_Firmware_Library\CMSIS" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\libraries\GD32F4xx_Firmware_Library\GD32F4xx_standard_peripheral\Include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\libraries\gd32_drivers" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\env_support\rt-thread" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\core" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\arm2d" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\gd32_ipa" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\nxp\pxp" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\nxp\vglite" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\nxp" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\sdl" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\stm32_dma2d" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\swm341_dma2d" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw\sw" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\draw" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\font" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\hal" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\layouts\flex" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\layouts\grid" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\barcode" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\bmp" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\ffmpeg" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\freetype" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\fsdrv" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\gif" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\png" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\qrcode" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\rlottie" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\sjpg" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\libs\tiny_ttf" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\misc" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\file_explorer" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\fragment" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\gridnav" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\ime" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\imgfont" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\monkey" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\msg" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\others\snapshot" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\themes\basic" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\themes\default" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\themes\mono" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\themes" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\animimg" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\arc" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\bar" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\btnmatrix" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\btn" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\calendar" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\canvas" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\chart" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\checkbox" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\colorwheel" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\dropdown" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\imgbtn" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\img" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\keyboard" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\label" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\led" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\line" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\list" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\menu" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\meter" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\msgbox" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\objx_templ" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\roller" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\slider" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\span" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\spinbox" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\spinner" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\switch" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\table" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\tabview" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\textarea" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\tileview" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src\widgets\win" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\packages\LVGL-latest\src" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\drivers\include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\finsh" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\compilers\common\include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\compilers\newlib" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\posix\io\poll" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\posix\io\stdio" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\components\libc\posix\ipc" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\include" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\libcpu\arm\common" -I"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rt-thread\libcpu\arm\cortex-m4" -include"E:\2_Projects\20221115_GD32F427_Trial_jishu\08_Workspace\gd32f427v_pluse\rtconfig_preinc.h" -std=gnu11 -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -Dgcc -O0 -gdwarf-2 -g -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

